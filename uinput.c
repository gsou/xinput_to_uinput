#include "uinput.h"
#include <errno.h>
#include <fcntl.h>
#include <linux/input.h>
#include <linux/uinput.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>

int uinput_event(int fd, unsigned short type, unsigned short code, int value) {
    struct input_event event[2] = {0};
    event[0].type = type;
    event[0].code = code;
    event[0].value = value;
    event[1].type = EV_SYN;

    if (write(fd, &event[0], sizeof(event)) != sizeof(event)) {
        printf("Error writing event.\n");
        return -1;
    }
    fsync(fd);
    return 0;
}

int uinput_open()
{
    struct uinput_user_dev  dev;
    int fd;
    size_t i;

    fd = open("/dev/uinput", O_WRONLY | O_SYNC | O_FSYNC);
    if (fd == -1) return -1;

    memset(&dev, 0, sizeof dev);
    strncpy(dev.name, "XINPUT_TO_UINPUT", UINPUT_MAX_NAME_SIZE);

    dev.id.bustype = BUS_USB;
    dev.id.vendor  = 1;
    dev.id.product = 1;
    dev.id.version = 1;

    if (ioctl(fd, UI_SET_EVBIT, EV_KEY) == -1) goto fail;
    if (ioctl(fd, UI_SET_EVBIT, EV_SYN) == -1) goto fail;
    if (ioctl(fd, UI_SET_EVBIT, EV_ABS) == -1) goto fail;

    for (i = 0; i < 0x200; i++)
      if (ioctl(fd, UI_SET_KEYBIT, i) == -1) goto fail;

    /* if (ioctl(fd, UI_SET_ABSBIT, ABS_X) == -1) goto fail; */
    /* if (ioctl(fd, UI_SET_ABSBIT, ABS_Y) == -1) goto fail; */

    if (write(fd, &dev, sizeof dev) != sizeof dev) goto fail;

    if (ioctl(fd, UI_DEV_CREATE) == -1) goto fail;

    return fd;

    fail:
    {
        const int saved_errno = errno;
        close(fd);
        errno = saved_errno;
        return -1;
    }
}

void uinput_close(const int fd)
{
    ioctl(fd, UI_DEV_DESTROY);
    close(fd);
}
