#ifndef UINPUT_H_
#define UINPUT_H_


int uinput_open();
int uinput_event(int fd, unsigned short type, unsigned short code, int value);
void uinput_close(const int fd);


#endif // UINPUT_H_
