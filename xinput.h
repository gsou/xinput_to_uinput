#ifndef XINPUT_H_
#define XINPUT_H_

#include <X11/Xlib.h>
#include <X11/extensions/XInput2.h>

int xinput_init();

int xinput_event(int* evtype, XIRawEvent* data);


#endif // XINPUT_H_
