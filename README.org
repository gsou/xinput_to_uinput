Some keyboard automation applications have support for X11 input but not for Wayland. this software provides a simple way to connect such an application to a uinput virtual device. By running the application in an enclosed X11 session, the application is free to send its global events, and Wayland can simply interact with uinput.

#+begin_src bash
xpra start :102
DISPLAY=:102 x11-only-automation-app
gcc main.c uinput.c xinput.c -lX11 -lXi
DISPLAY=:102 ./a.out
#+end_src

Currently, only keyboard events are supported, and mapping from Xinput to Uinput keycodes is incomplete.
