#include "uinput.h"
#include "xinput.h"
#include <X11/extensions/XInput2.h>
#include <errno.h>
#include <linux/input-event-codes.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>



static int remap_key(int key) {
    // Special characters
    if (key == 115) return KEY_LEFTMETA;
    // Directional keys
    if (key == 98) return KEY_UP;
    if (key == 99) return KEY_PAGEUP;
    if (key == 100) return KEY_LEFT;
    if (key == 102) return KEY_RIGHT;
    if (key == 104) return KEY_DOWN;
    if (key == 105) return KEY_PAGEDOWN;
    return key - 8;

}

static int remap_btn(int btn) {
  if (btn == 1) return BTN_LEFT;
  if (btn == 2) return BTN_MIDDLE;
  if (btn == 3) return BTN_RIGHT;
  return 0;
}

int main() {

    int fd = uinput_open();

    if (fd < 0) {
        printf("Could not connect to uinput: %s\n", strerror(errno));
    } else {
        printf("Connection success.\n");
    }

    if (xinput_init()) {
        printf("Could not connect to xinput\n");
    }
    for(;;) {
      int etype;
      XIRawEvent ev;
      if (xinput_event(&etype, &ev)) {
          int code = ev.detail;
          if (etype == 13) {
            // Press
            uinput_event(fd, EV_KEY, remap_key(code), 1);
            printf("EVENT %i - %i [%i]\n", etype, code, remap_key(code));
          } else if (etype == 14) {
            // Release
            uinput_event(fd, EV_KEY, remap_key(code), 0);
            printf("EVENT %i - %i [%i]\n", etype, code, remap_key(code));
          } else if (etype == 15) {
            // Button press
            uinput_event(fd, EV_KEY, remap_btn(code), 1);
            printf("EVENT %i - %i [%i]\n", etype, code, remap_btn(code));
          } else if (etype == 16) {
            // Button release
            uinput_event(fd, EV_KEY, remap_btn(code), 0);
            printf("EVENT %i - %i [%i]\n", etype, code, remap_btn(code));
          }
      }
    }

    uinput_close(fd);

}
