#include "xinput.h"
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/extensions/XInput2.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static int xi_opcode;

static Display * dis;
static int screen;
static Window win;
static GC gc;
static XIEventMask mask[1] = {0};

int xinput_init() {
    dis = XOpenDisplay((char *) 0);
    win = DefaultRootWindow(dis);

    mask[0].deviceid = XIAllMasterDevices;
    mask[0].mask_len = XIMaskLen(XI_LASTEVENT);
    mask[0].mask = calloc(mask[0].mask_len, sizeof(char));
    XISetMask(mask[0].mask, XI_RawKeyPress);
    XISetMask(mask[0].mask, XI_RawKeyRelease);
    XISetMask(mask[0].mask, XI_RawButtonPress);
    XISetMask(mask[0].mask, XI_RawButtonRelease);

    XISelectEvents(dis, win, &mask[0], 1);
    XSync(dis, 0);

    int event, error;
    if (!XQueryExtension(dis, "XInputExtension", &xi_opcode, &event, &error)) {
        printf("X Input Extension not available.\n");
        return -1;
    }

    return 0;
}

int xinput_event(int* evtype, XIRawEvent* data) {
    int ret = 0;
    XEvent event;
    XNextEvent(dis, &event);
    XGenericEventCookie *cookie = (XGenericEventCookie *)&event.xcookie;
    if (XGetEventData(dis, cookie) && cookie->type == GenericEvent && cookie->extension == xi_opcode) {
        *evtype = cookie->evtype;
        memcpy(data, cookie->data, sizeof(XIRawEvent));
        ret = 1;
        /* XKeycodeToKeysym(dis, data->detail, 0); */
    }
    XFreeEventData(dis, cookie);
    return ret;

}
